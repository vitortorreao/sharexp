package lmvdev.sharexp.ui;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.telly.floatingaction.FloatingAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

import lmvdev.sharexp.R;
import lmvdev.sharexp.utils.SXP_Request;
import lmvdev.sharexp.utils.SXP_ToastDisplayer;
import lmvdev.sharexp.ui.adapter.SalesListViewAdapter;
import lmvdev.sharexp.model.Sale;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

public class SalesFragment extends Fragment implements View.OnClickListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        ProgressBarActivity {

    public static final int MIN_SLICE = 15;

    private static boolean pushToUpload = false;

    ListView salesList;
    SalesListViewAdapter adapter;
    Vector<Sale> sales;

    LocationClient locationClient;
    Location currentLocation;

    FloatingAction floatingAction;
    PullToRefreshLayout pullToRefreshLayout;

    int offset;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Toast.makeText(this.getActivity(), "onCreate", Toast.LENGTH_SHORT).show();
        locationClient = new LocationClient(this.getActivity(), this, this);
        //locationClient.connect();

        offset = 0;

    }

    public void customLoadMoreDataFromApi(int offset) {
        // This method probably sends out a network request and appends new data items to your adapter.
        // Use the offset value and add it as a parameter to your API request to retrieve paginated data.
        // Deserialize API response and then construct new objects to append to the adapter
        Log.d("Sxp/Scroll","Offset: " + offset);
        new SXP_Request("Sxp/SalesList", SalesFragment.this).execute(String.format(
                "http://share-xp.appspot.com/sales?lat=%f&lon=%f&offset=%d",
                currentLocation.getLatitude(), currentLocation.getLongitude(), offset));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("Sxp/SalesFragment", "CREATED VIEW");

        //Toast.makeText(this.getActivity(), "onCreateView", Toast.LENGTH_SHORT).show();

        final View rootView = inflater.inflate(R.layout.fragment_sales, container, false);

        salesList = (ListView) rootView.findViewById(R.id.sales_listView);

        // Attach the listener to the AdapterView onCreate
        salesList.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                if (sales.size() >= MIN_SLICE) {
                    offset++;
                    customLoadMoreDataFromApi(offset);
                }
                // or customLoadMoreDataFromApi(totalItemsCount);
            }
        });

        // Now find the PullToRefreshLayout to setup
        pullToRefreshLayout = (PullToRefreshLayout) rootView.findViewById(R.id.ptr_layout);

        // Now setup the PullToRefreshLayout
        ActionBarPullToRefresh.from(getActivity())

                .allChildrenArePullable()
                        // Set a OnRefreshListener
                .listener(new OnRefreshListener() {
                    @Override
                    public void onRefreshStarted(View view) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

//                                sales.clear();
//                                adapter.notifyDataSetChanged();
//
//                                //reset offset to zero
                                offset = 0;

                                pushToUpload = true;

                                new SXP_Request("Sxp/SalesList", SalesFragment.this).execute(String.format(
                                        "http://share-xp.appspot.com/sales?lat=%f&lon=%f&offset="+ getOffset(),
                                        currentLocation.getLatitude(), currentLocation.getLongitude()));
                            }
                        });
                    }
                })


                // Finally commit the setup to our PullToRefreshLayout
                .setup(pullToRefreshLayout);


//        Button createButton = (Button) rootView.findViewById(R.id.createButton);
//        createButton.setOnClickListener(this);
//
//        Button refineButton = (Button) rootView.findViewById(R.id.refineButton);
//        refineButton.setOnClickListener(this);

        showFloatActionButton(); //THIS IS BECAUSE WE DELETED THE NOTIFICATIONS FRAGMENT

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Connect the client.
        locationClient.connect();
        Log.d("Sxp/SalesFragment", "STARTED");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        Log.d("Sxp/SalesFragment", "LOW MEMORY");
    }

    @Override
    public void onStop() {
        super.onStop();

        // Disconnecting the client invalidates it.
        locationClient.disconnect();

        Log.d("Sxp/SalesFragment", "STOPPED");
    }

    public void showFloatActionButton() {
        floatingAction = FloatingAction.from(getActivity())
                .listenTo(salesList)
                .colorResId(R.color.projectColor)
                .icon(R.drawable.ic_add)
                .listener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        create();
                    }
                }).build();
    }

    public void hideFloatActionButton() {
        if(this.floatingAction != null) {
            this.floatingAction.hide();
            this.floatingAction.onDestroy();
            this.floatingAction = null;
        }
    }

    public void getSales(JSONArray json) {

        //Toast.makeText(this.getActivity(), "getSale",Toast.LENGTH_SHORT).show();

        final JSONArray json2 = json; //So it is accessible inside the UiThread

        if(this.getActivity() == null) {
            return;
        }

        this.getActivity().runOnUiThread( new Runnable() {
            @Override
            public void run() {

                try {

                    if(pushToUpload && sales != null && adapter != null) {
                        sales.clear();
                        adapter.notifyDataSetChanged();

                        //reset offset to zero
                        offset = 0;

                        pushToUpload = false;
                    } else if(pushToUpload) {
                        pushToUpload = false;
                    }

                    if(sales==null)
                        sales = new Vector<Sale>();

                    for (int i = 0; i< json2.length(); i++) {
                        sales.add(new Sale (json2.getJSONObject(i)));
                    }

                    Log.d("Sxp/Scroll","Length: " + sales.size());


                } catch(Exception e) {

                    return;
                }

                if(salesList.getAdapter()==null) {

                    if(getActivity() == null) {
                        return;
                    }
                    adapter = new SalesListViewAdapter(getActivity(), sales, currentLocation);
                    salesList.setAdapter(adapter);
                    salesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, final View view,
                                                int position, long id) {


                            Log.d("Sxp/Scroll","You just clicked on " + position + " (out of " + sales.size() + ")");

                            //Call Sale Details Activity
                            Intent detailsIntent = new Intent(getActivity(), SaleDetailsActivity.class);
                            detailsIntent.putExtra("sale", sales.get(position));
                            startActivity(detailsIntent);

                        }

                    });
                }

                adapter.notifyDataSetChanged();

            }

        });


    }

    public void create() {
        //Toast.makeText(getActivity(), "Create new sale",Toast.LENGTH_SHORT).show();

        //Call Create Sale Activity
        if(this.getActivity() == null) {
            return;
        }
        Intent createSaleIntent = new Intent(getActivity(), CreateSaleActivity.class);
        startActivity(createSaleIntent);

    }

    public void refine() {
        if(this.getActivity() == null) {
            return;
        }
        Toast.makeText(getActivity(), "Refine sales",Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d("Sxp/SalesFragment", "DESTROYED");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createButton:
                create();
                break;
            case R.id.refineButton:
                refine();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d("Sxp/SalesFragment", "PAUSED");
    }

    @Override
    public void onResume(){
        super.onResume();

        offset = 0;
        Log.d("Sxp/SalesFragment", "RESUMED");
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        // Display the connection status
        //Toast.makeText(this.getActivity(), "Connected", Toast.LENGTH_SHORT).show();

        Log.d("Sxp/SalesFragment", "CONNECTED TO GPS");

        try {

            currentLocation = locationClient.getLastLocation();
//          Toast.makeText(this.getActivity(), "Current: " + currentLocation.getLatitude() + " " +
//                currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();

            if (sales != null) {
                sales.clear();
                adapter.notifyDataSetChanged();

                //reset offset to zero
                offset = 0;
            }

            new SXP_Request("Sxp/SalesList", this).execute(String.format(
                    "http://share-xp.appspot.com/sales?lat=%f&lon=%f&offset=" + getOffset(),
                    currentLocation.getLatitude(), currentLocation.getLongitude()));

        } catch (Exception e) {
            if(this.getActivity() == null) {
                return;
            }
            Toast.makeText(this.getActivity(), "Location not available",Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        Log.d("Sxp/SalesFragment", "DISCONNECTED FROM GPS");

        // Display the connection status
        if(this.getActivity() == null) {
            return;
        }
        Toast.makeText(this.getActivity(), "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();

    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this.getActivity(),
                        MainActivity.CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */

            //TODO: display an error message to user

            //showErrorDialog(connectionResult.getErrorCode());
        }

    }

    @Override
    public void setProgressBarFalse() {
        if(this.getActivity() != null) {
            ((ProgressBarActivity) this.getActivity()).setProgressBarFalse();
        }
    }

    @Override
    public void setProgressBarTrue() {
        if(this.getActivity() != null) {
            ((ProgressBarActivity) this.getActivity()).setProgressBarTrue();
        }
    }

    @Override
    public void onTaskFinish(String result) {

        try {
            JSONObject json = new JSONObject(result);

            if (json.getBoolean("status"))
               getSales(json.getJSONArray("sales"));
            else
                this.getActivity().runOnUiThread(new SXP_ToastDisplayer(this.getActivity(),
                        "Error: Location not available"));

            if(getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pullToRefreshLayout.setRefreshing(false);
                    }
                });
            }

        } catch (JSONException e) {
            if(this.getActivity() == null) {
                return;
            }
            this.getActivity().runOnUiThread(new SXP_ToastDisplayer(this.getActivity(), "Error: " +
                    e.getMessage()));
        }

    }

    public int getOffset() {
        return offset;
    }

    //    /*
//     * Called when the Activity becomes visible.
//     */
//    @Override
//    public void onStart() {
//        super.onStart();
//
//        Toast.makeText(this.getActivity(), "onStart", Toast.LENGTH_SHORT).show();
//
//        // Connect the client.
//        locationClient.connect();
//    }

//    /*
//     * Called when the Activity is no longer visible.
//     */
//    @Override
//    public void onStop() {
//
//        Toast.makeText(this.getActivity(), "onStop", Toast.LENGTH_SHORT).show();
//
//        // Disconnecting the client invalidates it.
//        locationClient.disconnect();
//        super.onStop();
//    }




}