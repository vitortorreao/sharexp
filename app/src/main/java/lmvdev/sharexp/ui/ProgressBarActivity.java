package lmvdev.sharexp.ui;

import org.json.JSONObject;

public interface ProgressBarActivity {

    public void setProgressBarFalse();

    public void setProgressBarTrue();

    public void onTaskFinish(String result);

}
