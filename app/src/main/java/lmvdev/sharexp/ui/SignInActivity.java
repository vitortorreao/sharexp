package lmvdev.sharexp.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import lmvdev.sharexp.R;
import lmvdev.sharexp.utils.SXP_Request;
import lmvdev.sharexp.utils.SXP_ToastDisplayer;
import lmvdev.sharexp.utils.SXP_Utils;

public class SignInActivity extends Activity implements ProgressBarActivity {

    boolean hasErrors;
    SXP_Request sxp_request;

    private static String URL = "http://share-xp.appspot.com/login?";
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_signin);
        setProgressBarIndeterminateVisibility(false);
        hasErrors = false;
    }

    public void displayError(TextView textView, String message) {
        textView.setError(message);
        hasErrors = true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, AccessActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, AccessActivity.class);
                startActivity(intent);
                this.finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public void signIn(View view) {

        hasErrors = false;

         TextView email = (TextView) findViewById(R.id.signin_email);
        TextView password = (TextView) findViewById(R.id.signin_password);

        if (TextUtils.isEmpty(email.getText()))
            displayError(email,"This is a mandatory field!");
        else if (!RegisterActivity.isValidEmail(email.getText()))
            displayError(email,"Invalid e-mail!");

        if (TextUtils.isEmpty(password.getText()))
            displayError(password,"This is a mandatory field!");

        Log.d("Sxp/register", "Pressed sign in button");
        Log.d("Sxp/register", "Inputs: " + email.getText() + ", " + password.getText());
        Log.d("Sxp/register", "Errors:" + hasErrors);

        String URL_final = URL+
                "username="+ SXP_Utils.prepareParameter(email.getText().toString()) +"&"+
                "password="+ SXP_Utils.prepareParameter(password.getText().toString());

        Log.d("SXP_URL", URL_final);

        setProgressBarIndeterminateVisibility(true);

        sxp_request = new SXP_Request("SXP/Login", this);
        sxp_request.execute(URL_final);

    }

    @Override
    public void setProgressBarFalse() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(false);
            }
        });

    }

    @Override
    public void setProgressBarTrue() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(true);
            }
        });

    }

    @Override
    public void onTaskFinish(String resultString) {

        try {

            JSONObject json = new JSONObject(resultString);
            if(json.getBoolean("status")) {
                runOnUiThread(new SXP_ToastDisplayer(this, "Login Successful!"));
                String name = json.getString("username");
                mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putBoolean(MainActivity.PREF_LOGGED, true);
                editor.putString(MainActivity.PREF_NAME, name);
                editor.putString(MainActivity.PREF_COOKIE, sxp_request.getCookie());
                editor.commit();

                //Call Home Activity
                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                finish();
            } else {
                runOnUiThread(new SXP_ToastDisplayer(this, "Invalid e-mail/password!"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
