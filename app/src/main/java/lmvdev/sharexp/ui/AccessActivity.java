package lmvdev.sharexp.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.model.GraphUser;

import org.json.JSONException;
import org.json.JSONObject;

import lmvdev.sharexp.R;
import lmvdev.sharexp.utils.SXP_Request;
import lmvdev.sharexp.utils.SXP_ToastDisplayer;

public class AccessActivity extends Activity implements ProgressBarActivity{

    private Button buttonFbLogin;
    private Session.StatusCallback statusCallback = new SessionStatusCallback();

    SharedPreferences mSharedPreferences;

    public static String someAccessToken = "";
    public static String correctAccessToken = "";
    public static boolean usedFacebookLogin = false;
    //I'm not proud of it either.

    //^ that's vitor stuff, weeeirdo

    SXP_Request sxp_request;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access);

        buttonFbLogin = (Button) findViewById(R.id.fb_button);

        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

        Session session = Session.getActiveSession();

        Log.d("Sxp/Login", "Session: "+session);

        if (session == null) {
            if (savedInstanceState != null) {
                session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
            }
            if (session == null) {
                session = new Session(this);
            }
            Session.setActiveSession(session);
            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
            }
        }

        buttonFbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { onClickLogin(); usedFacebookLogin=true; }
        });


    }

    private void onClickLogin() {
        Session session = Session.getActiveSession();
        if (!session.isOpened() && !session.isClosed()) {
            session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
        } else {
            Session.openActiveSession(this, true, statusCallback);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(usedFacebookLogin == false){
            finish();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }


    //What happens when the user clicks on the "Sign up with email" button
    public void register(View view) {
        Log.d("LoginView", "Register!");

        //Call Register Activity
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    public void moveToHome() {

        String url = "http://share-xp.appspot.com/login?access_token="+correctAccessToken;
        Log.d("SXP+Facebook/Login", url);

        sxp_request = new SXP_Request("SXP+Facebook/Login", this);
        sxp_request.execute(url);


    }

    //What happens when the user clicks on the "Log In" button
    public void login(View view) {

        //Call Home Activity
        Intent SignInIntent = new Intent(this, SignInActivity.class);
        SignInIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(SignInIntent);
        Log.d("LoginView", "Log In!");
    }





    @Override
    public void setProgressBarFalse() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    @Override
    public void setProgressBarTrue() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(true);
            }
        });
    }

    @Override
    public void onTaskFinish(String result) {

        mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(MainActivity.PREF_LOGGED, true);

        //Get user's name
        String name = "";
        try {
            JSONObject json = new JSONObject(result);
            name = json.getString("username");
        } catch (JSONException e) {
            runOnUiThread(new SXP_ToastDisplayer(this, "There was a problem on the server. Try again later"));
        }



        editor.putString(MainActivity.PREF_NAME, name);
        editor.putString(MainActivity.PREF_COOKIE, sxp_request.getCookie());
        editor.commit();


        //Call Home Activity
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        finish();

    }

    private class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {

            if(session != null) {

                //What happens when the user chooses to log in through Facebook
                Log.d("Facebook/Login", "Logged in with token: " + session.getAccessToken());
                someAccessToken = session.getAccessToken();
                //Request user full name
                Request.newMeRequest(session, new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        if(user != null) {
                            Log.d("Facebook/Login", "Logged in as "+user.getName());
                            setProgressBarIndeterminateVisibility(true);
                            setCorrectAccessToken();

                            moveToHome();

                        }
                    }

                }).executeAsync();
            } else {
                Log.d("Facebook/Login", "Session is null!");
            }

        }
    }

    public static void setCorrectAccessToken() {
        correctAccessToken = someAccessToken.concat("");
        //To guarantee it is not a reference operation
    }

}
