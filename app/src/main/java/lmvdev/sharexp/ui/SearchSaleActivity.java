package lmvdev.sharexp.ui;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

import lmvdev.sharexp.R;
import lmvdev.sharexp.model.Sale;
import lmvdev.sharexp.ui.adapter.SalesListViewAdapter;

public class SearchSaleActivity extends Activity implements GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    private static final String URL = "http://share-xp.appspot.com/hashtag_search?tag=";

    private ListView mSalesListView;
    private SalesListViewAdapter mAdapter;
    private Vector<Sale> mSales;
    private String mQuery;
    private LocationClient mLocationClient;
    private Location mCurrentLocation;
    private String mCookie;
    SharedPreferences mSharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_sale);

        Log.d("Sxp/Search", "here");

        Intent intent = getIntent();

        Log.d("Sxp/Search", "Intent: "+intent.toString());

        Log.d("Sxp/Search", "Intent Action: "+intent.getAction());
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            mQuery = intent.getStringExtra(SearchManager.QUERY);
        }

        mSalesListView = (ListView) findViewById(R.id.search_sale_results);

        mLocationClient = new LocationClient(this, this, this);

        mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);
        mCookie = mSharedPreferences.getString(MainActivity.PREF_COOKIE, null);

    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//
//        setIntent(intent);
//        handleIntent(intent);
//
//        super.onNewIntent(intent);
//    }
//
//    private void handleIntent(Intent intent) {
//
//
//    }

    private void doMySearch() {

        Log.d("Sxp/Search", "Inside doMySearch. Query: "+mQuery+". Cookie: "+mCookie);

        Ion.with(this)
                .load(URL + mQuery)
                .setHeader("Cookie", "id=" + mCookie)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Log.d("Sxp/Search", "Result: " + result.toString());

                        try {

                            JSONObject json = new JSONObject(result);
                            if (json.getBoolean("status")) {

                                JSONArray results = json.getJSONArray("results");

                                for(int i = 0; i < results.length(); i++) {
                                        Sale sale = new Sale(results.getJSONObject(i));
                                        mSales.add(sale);
                                        mAdapter.notifyDataSetChanged();
                                }

                            } else {
                                Toast.makeText(SearchSaleActivity.this,
                                        "Error: " + json.getString("reason"),
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e1) {
                            Log.e("Sxp/Sale", e1.getMessage());
                        }

                    }
                });

    }

    @Override
    public void onStart() {
        super.onStart();

        // Connect the client.
        mLocationClient.connect();
    }

    @Override
    public void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();

        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

        try {

            mCurrentLocation = mLocationClient.getLastLocation();
//          Toast.makeText(this.getActivity(), "Current: " + currentLocation.getLatitude() + " " +
//                currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();

            if (mSales != null) {
                mSales.clear();
                mAdapter.notifyDataSetChanged();
            }

            mSales = new Vector<Sale>();

            mAdapter = new SalesListViewAdapter(this, mSales, mCurrentLocation);

            mSalesListView.setAdapter(mAdapter);
            mSalesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {


                    Log.d("Sxp/SearchScroll", "You just clicked on " + position + " (out of " + mSales.size() + ")");

                    //Call Sale Details Activity
                    Intent detailsIntent = new Intent(SearchSaleActivity.this, SaleDetailsActivity.class);
                    detailsIntent.putExtra("sale", mSales.get(position));
                    startActivity(detailsIntent);

                }

            });

            Log.i("Sxp/Search", "Query: "+mQuery);

            doMySearch();

        } catch (Exception e) {
            Toast.makeText(this, "Location not available", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onDisconnected() {
        Toast.makeText(this, "Disconnected from GPS. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        MainActivity.CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */

            //TODO: display an error message to user

            //showErrorDialog(connectionResult.getErrorCode());
        }

    }
}
