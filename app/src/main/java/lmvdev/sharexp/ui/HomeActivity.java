package lmvdev.sharexp.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.Session;

import java.util.List;

import lmvdev.sharexp.R;
import lmvdev.sharexp.utils.SXP_Request;
import lmvdev.sharexp.ui.adapter.TabsPagerAdapter;

public class HomeActivity extends ActionBarActivity implements ActionBar.TabListener, ProgressBarActivity {

    SharedPreferences mSharedPreferences;
    private Session session;
    private static String userName = "";

    //protected int inflateMenu = R.menu.main;
    //THIS IS BECAUSE WE HAVE DELETED THE NOTIFICATIONS TAB
    protected int inflateMenu = R.menu.sales_menu;

    private ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    // Tab titles
    private String[] tabs = {"Sales", "Profile"};
    private int[] icons = {R.drawable.ic_sales, R.drawable.ic_profile};

    public static void setUserName(String name) {
        userName = name;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Initialization
        viewPager = (ViewPager) findViewById(R.id.pager);
        actionBar = getSupportActionBar();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(mAdapter);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

//        // Adding Tabs
//        for (String tab_name : tabs) {
//            actionBar.addTab(actionBar.newTab().setText(tab_name)
//                    .setTabListener(this));
//        }

        // Adding Tabs
        int i = 0;
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setIcon(icons[i])
                    .setTabListener(this));
            i++;
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {



            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                actionBar.setSelectedNavigationItem(position);

                Log.d("Sxp/ChangeTab","Position: "+ position);
                if (position == 0) {
                    inflateMenu = R.menu.sales_menu;
                    List<Fragment> list  = getSupportFragmentManager().getFragments();
                    Log.d("Sxp/ChangeTab", ""+getSupportFragmentManager().getFragments());
                    for (Fragment fragment : list){
                        if(fragment instanceof SalesFragment){
                            SalesFragment salesFragment = (SalesFragment) fragment;
                            salesFragment.showFloatActionButton();
                        }
                    }
                } else {
                    List<Fragment> list  = getSupportFragmentManager().getFragments();
                    for (Fragment fragment : list){
                        if(fragment instanceof SalesFragment){
                            SalesFragment salesFragment = (SalesFragment) fragment;
                            salesFragment.hideFloatActionButton();
                        }
                    }
                    inflateMenu = R.menu.main;
                }
                invalidateOptionsMenu();

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

//        mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);
//        String name = mSharedPreferences.getString(MainActivity.PREF_NAME, "");
//
//        TextView welcomeText = (TextView) findViewById(R.id.welcome_home);
//        welcomeText.setText(name);


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d("Sxp/ChangeMenu","Menu: " + menu.toString());
        menu.clear();
        getMenuInflater().inflate(inflateMenu, menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu.
        // Adds items to the action bar if it is present.
        getMenuInflater().inflate(inflateMenu, menu);

        // Access the Item defined in menu XML
        MenuItem logout = menu.findItem(R.id.action_logout);

        if(logout != null)
        {

        }

        // Access the object responsible for

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_logout:
                logout();
                break;
            case R.id.sale_search:
                //Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
                return onSearchRequested();
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    public void logout() {

        mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(MainActivity.PREF_LOGGED, false);
        editor.commit();

        Session session = Session.getActiveSession();
        if(session == null) {
            session = new Session(this);
            Session.setActiveSession(session);
        }
        session.closeAndClearTokenInformation();
//        if(session != null && !session.isClosed()) {
//            session.closeAndClearTokenInformation();
//            Log.d("SXP+Facebook/Logout", "Logout ok!");
//        }

        new SXP_Request("SXP+Facebook/Logout", this, mSharedPreferences.getString(MainActivity.PREF_COOKIE,null)).execute("http://share-xp.appspot.com/logout");

        //Call Access Activity
        Intent accessIntent = new Intent(this, AccessActivity.class);
        accessIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(accessIntent);
        finish();

    }

    @Override
    public void setProgressBarFalse() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    @Override
    public void setProgressBarTrue() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(true);
            }
        });
    }

    @Override
    public void onTaskFinish(String result) {

    }
}
