package lmvdev.sharexp.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import lmvdev.sharexp.R;
import lmvdev.sharexp.model.Person;
import lmvdev.sharexp.utils.SXP_ToastDisplayer;

public class ProfileFragment extends Fragment {

    private static final String PROFILE_URL = "http://share-xp.appspot.com/profile";
    private SharedPreferences mSharedPreferences;
    private String cookie;
    private View profileView;
    private Person user;

    private int numReports;
    private int numSales;
    private int numReportsMade;
    private String fbId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mSharedPreferences = getActivity().getSharedPreferences(MainActivity.PREFS,
                getActivity().MODE_PRIVATE);

        cookie = mSharedPreferences.getString(MainActivity.PREF_COOKIE, null);

        Ion.with(getActivity())
                .load(PROFILE_URL)
                .setHeader("Cookie", "id=" + cookie)
                .asString()
                .setCallback(new FutureCallback<String>() {

                    @Override
                    public void onCompleted(Exception e, String s) {

                        try {
                            JSONObject json = new JSONObject(s);
                            if(json.getBoolean("status")) {
                                JSONObject profile = json.getJSONObject("profile");
                                user = new Person(profile);
                                numReports = profile.getInt("num_reports");
                                numSales = profile.getInt("num_sales");
                                numReportsMade = profile.getInt("num_reports_made");
                                fbId = profile.getString("fb_id");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        fillProfile();
                                    }
                                });
                            } else {
                                String reason = json.getString("reason");
                                getActivity().runOnUiThread(
                                        new SXP_ToastDisplayer(getActivity(), reason)
                                );
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    }

                });

        profileView = rootView;

        return rootView;
    }

    private void fillProfile() {

        if(!this.fbId.equals("null")) {
            ImageView profilePicture = (ImageView) profileView.findViewById(R.id.ProfileFragment_ProfileImage);
            String url = "https://graph.facebook.com/"+this.fbId+"/picture?height=1000&width=1000";
            Log.d("Sxp/Profile", "URL: "+url);
            Picasso.with(getActivity())
                    .load(url)
                    .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                    .error(R.drawable.com_facebook_profile_picture_blank_square)
                    .into(profilePicture);
        }

        TextView name = (TextView) profileView.findViewById(R.id.profile_firstName);
        TextView email = (TextView) profileView.findViewById(R.id.profile_email);
        TextView location = (TextView) profileView.findViewById(R.id.profile_location);
        TextView numSales = (TextView) profileView.findViewById(R.id.profile_numberOfSales);
        TextView numReports = (TextView) profileView.findViewById(R.id.profile_numberOfReports);
        TextView numReportsMade = (TextView) profileView
                .findViewById(R.id.profile_numberOfReportsMade);

        String[] names = user.getName().split(" ");

        name.setText(user.getName());

        email.setText("Email: "+user.getEmail());
        if(!user.getLocation().equals("null")) {
            location.setText("Location: "+user.getLocation());
        } else {
            location.setText("Location: ");
        }

        String wasWere=" was";
        if(this.numReports > 1){
           wasWere=" were";
        }

        numSales.setText(user.getName().split(" ")[0]+" created "+this.numSales+" sales");
        numReports.setText("of which "+this.numReports+wasWere+" reported.");

        numReportsMade.setText(user.getName().split(" ")[0]+" also reported "
                +this.numReportsMade+" sales.");

    }

}