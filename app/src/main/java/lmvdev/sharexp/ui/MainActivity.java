package lmvdev.sharexp.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;


public class MainActivity extends Activity {

    //Name of the set of preferences
    public static final String PREFS = "prefs";

    //boolean value: if the user is logged in or not
    public static final String PREF_LOGGED = "logged";
    //String value: the user's name
    public static final String PREF_NAME = "name";
    //String value: the user's cookie
    public static final String PREF_COOKIE = "cookie";

    //Define a request code to send to Google Play services. This code is returned in Activity.onActivityResult
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;


    SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);


        //SHOW FACEBOOK HASHKEY
//        PackageInfo info = null;
//
//        try {
//            info = getPackageManager().getPackageInfo(getPackageName(),  PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures)
//            {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch(Exception e) {
//            e.printStackTrace();
//        }

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);

        //Check if the user is logged in
        boolean isLogged = mSharedPreferences.getBoolean(PREF_LOGGED, false);

        if(isLogged) { //If the user is logged in
            Log.d("Logged", "User is already logged in.");

            //Call Home Activity
            Intent homeIntent = new Intent(this, HomeActivity.class);
            startActivity(homeIntent);

        } else { //If the user uses our login system
            Log.d("Logged", "User is not logged in.");

            //Call Access Activity
            Intent accessIntent = new Intent(this, AccessActivity.class);
            startActivity(accessIntent);
        }

    }

    /*
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {

        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    /*
                     * Try the request again
                     */
                        break;
                }
        }
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        int errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (errorCode != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(errorCode, this, 0).show();
            return false;
        } else {
            Log.d("Location Updates",
                    "Google Play services is available.");
            return true;
        }
    }

}
