package lmvdev.sharexp.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import lmvdev.sharexp.R;
import lmvdev.sharexp.model.Sale;

public class SaleDetailsActivity extends Activity {

    Sale sale;
    TextView discountDescription;
    TextView originalPrice;
    TextView endDate;
    TextView address;
    TextView createdBy;
    TextView hashtags;
    GoogleMap map;
    ImageView productPicture;
    SharedPreferences mSharedPreferences;

    private static final String URL = "http://share-xp.appspot.com/like?sale_id=";

    ShareActionProvider mShareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_details);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        sale = (Sale) getIntent().getExtras().getSerializable("sale");

        this.setTitle(sale.getProduct().getName());

        originalPrice = (TextView) findViewById(R.id.saleDetails_originalPrice);
        discountDescription = (TextView) findViewById(R.id.saleDetails_discountDescription);
        address = (TextView) findViewById(R.id.saleDetails_address);
        productPicture = (ImageView) findViewById(R.id.saleDetails_productImage);
        endDate = (TextView) findViewById(R.id.saleDetails_endDate);
        createdBy = (TextView) findViewById(R.id.saleDetails_createdBy);
        hashtags = (TextView) findViewById(R.id.saleDetails_hashtags);

        originalPrice.setText("Before: " + String.format("£%.2f", sale.getOriginalPrice()));

        discountDescription.setText("Now: " + sale.getDiscountDescription());
        if (discountDescription.getText().toString().endsWith("%"))
            discountDescription.setText(discountDescription.getText().toString().concat(" off"));

        Calendar cal = Calendar.getInstance();
        cal.setTime(sale.getEndDate());
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        endDate.setText("Valid Until: " + format.format(cal.getTime()));

        createdBy.setText("Created By: " + sale.getOwner().getName());

        String joint_hashtags = "Hashtags: ";

        for (String hastag : sale.getHashtags()) {
            joint_hashtags += "#"+hastag+" ";
        }

        SpannableString hashtaglist = new SpannableString(joint_hashtags);
        final ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(R.color.projectColor));
        hashtaglist.setSpan(fcs, 10, joint_hashtags.length(), 0);

        hashtags.setText(hashtaglist);

        SpannableString addressUnderlined = new SpannableString(sale.getMerchant().getName() + ": " + sale.getMerchant().getAddress());
        addressUnderlined.setSpan(new UnderlineSpan(), sale.getMerchant().getName().length() + 2, addressUnderlined.length(), 0);
        address.setText(addressUnderlined);

        if (sale.getProduct().hasPictureUrl()) {
            Picasso.with(this)
                    .load(sale.getProduct().getPictureUrl())
                    .into(productPicture);

            productPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Call Photo View Activity
                    Intent photoIntent = new Intent(SaleDetailsActivity.this, PhotoViewActivity.class);
                    photoIntent.putExtra("product", sale.getProduct());
                    startActivity(photoIntent);
                }
            });
        } else
            productPicture.setImageResource(R.drawable.ic_sales_blue);

        mapInit();

        mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);

    }

    private void mapInit() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (map == null) {
            map= ((MapFragment) getFragmentManager().findFragmentById(R.id.saleDetails_map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (map != null) {
                // The Map is verified. It is now safe to manipulate the map.

                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(sale.getMerchant().getLatitude(),sale.getMerchant().getLongitude()),17));
                //map.animateCamera(CameraUpdateFactory.zoomTo(17), 5000, null);


                Marker locationMarker = map.addMarker(new MarkerOptions().position(new LatLng(sale.getMerchant().getLatitude(),sale.getMerchant().getLongitude())).title(sale.getMerchant().getName()));
                locationMarker.showInfoWindow();

            }
        }
    }


    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onMenuItemSelected(featureId, item);
    }

    private void setShareIntent() {

        // create an Intent with the content
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT,
                sale.getProduct().getName() + " on sale!");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Did you know that " + sale.getProduct().getName() +
                " is on sale at " + sale.getMerchant().getName() + "? Now it's " +
                sale.getDiscountDescription().toLowerCase() + ".");

        // Make sure the provider knows
        // it should work with that Intent
        mShareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu
        // this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sale_details, menu);

        // Access the Share Item defined in menu XML
        MenuItem shareItem = menu.findItem(R.id.sale_share);

        // Access the object responsible for
        // putting together the sharing submenu
        if (shareItem != null) {
            mShareActionProvider
                    = (ShareActionProvider) shareItem.getActionProvider();
        }

        setShareIntent();

        return true;
    }

    public void createMapIntent(View v) {

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://google.com/maps/place//@"+ sale.getMerchant().getLatitude()+","+
                sale.getMerchant().getLongitude()+",15z")   );
        startActivity(intent);

    }

    public void likeSale(View view) {

        Log.d("Sxp/LikeSale", "Clicked!");

        final String cookie = mSharedPreferences.getString(MainActivity.PREF_COOKIE, null);

        Ion.with(this)
                .load(URL + sale.getId())
                .setHeader("Cookie", "id=" + cookie)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        Log.d("Sxp/LikeSale", "Result:\n"+result.toString());

                        if(result.get("status").getAsBoolean()) {
                            Toast.makeText(SaleDetailsActivity.this,
                                    "You liked this",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SaleDetailsActivity.this,
                                    "Error: " + result.get("reason").getAsString(),
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }

    public void report(View view) {

        ReportDialogFragment dialog = new ReportDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("sale",sale);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(),"ReportDialog");

    }

}
