package lmvdev.sharexp.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import lmvdev.sharexp.R;
import lmvdev.sharexp.model.Sale;

public class ReportDialogFragment extends DialogFragment {

    private final String Url = "http://share-xp.appspot.com/report?sale_id=";
    private Sale sale;
    SharedPreferences mSharedPreferences;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        if(bundle != null){
            sale  = (Sale) bundle.getSerializable("sale");
        }

        mSharedPreferences = getActivity().getSharedPreferences(MainActivity.PREFS, getActivity().MODE_PRIVATE);
        final String cookie = mSharedPreferences.getString(MainActivity.PREF_COOKIE, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.report_dialog_title)
                .setItems(R.array.report_options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        final Context context = getActivity();

                        switch (which) {
                            case 0:
                                Ion.with(getActivity())
                                        .load(Url + sale.getId())
                                        .setHeader("Cookie", "id="+cookie)
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                Log.d("Sxp/Report","Result: " + result.toString());

                                                if (result.get("status").getAsBoolean()) {

                                                    Toast.makeText(context,
                                                            "Your report was successful",
                                                            Toast.LENGTH_SHORT).show();

                                                } else {
                                                    Toast.makeText(context,
                                                            "Error: " + result.get("reason").getAsString(),
                                                            Toast.LENGTH_SHORT).show();
                                                }


                                            }
                                        });
                                break;
//                            case 1:
//                                Toast.makeText(getActivity(),"Second option",Toast.LENGTH_SHORT).show();
//                                break;
                        }

                    }
                });
        return builder.create();
    }

}
