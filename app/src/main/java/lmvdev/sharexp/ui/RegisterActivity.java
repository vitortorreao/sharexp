package lmvdev.sharexp.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import lmvdev.sharexp.R;
import lmvdev.sharexp.utils.SXP_Request;
import lmvdev.sharexp.utils.SXP_ToastDisplayer;
import lmvdev.sharexp.utils.SXP_Utils;

public class RegisterActivity extends Activity implements ProgressBarActivity {

    private static String URL = "http://share-xp.appspot.com/register?";

    private static String reason = "";

    SXP_Request sxp_request;

    boolean hasErrors;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_register);
        setProgressBarIndeterminateVisibility(false);
        hasErrors = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, AccessActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, AccessActivity.class);
                startActivity(intent);
                this.finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }




    public void displayError(TextView textView, String message) {
        textView.setError(message);
        hasErrors = true;
    }

    @Override
    public void setProgressBarFalse() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(false);
            }
        });

    }

    @Override
    public void setProgressBarTrue() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(true);
            }
        });

    }

    @Override
    public void onTaskFinish(String resultString) {
        try {
            JSONObject json = new JSONObject(resultString);
            if(json.getBoolean("status")) {
                runOnUiThread(new SXP_ToastDisplayer(this, "Registration Successful!"));
                String name = json.getString("username");
                mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putBoolean(MainActivity.PREF_LOGGED, true);
                editor.putString(MainActivity.PREF_NAME, name);
                editor.putString(MainActivity.PREF_COOKIE, sxp_request.getCookie());
                editor.commit();

                //Call Home Activity
                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                finish();

            } else {
                runOnUiThread(new SXP_ToastDisplayer(this, json.getString("reason")));
            }
        } catch (JSONException e) {
            runOnUiThread(new SXP_ToastDisplayer(this, "There was a problem on the server. Try again later"));
        }
    }

    public void createUser(View view) {

        hasErrors = false;

        TextView firstName = (TextView) findViewById(R.id.register_firstName);
        TextView lastName = (TextView) findViewById(R.id.register_lastName);
        TextView email = (TextView) findViewById(R.id.register_email);
        TextView password = (TextView) findViewById(R.id.register_password);
        TextView passwordConf = (TextView) findViewById(R.id.register_passwordConf);
        TextView location = (TextView) findViewById(R.id.register_location);

        if (TextUtils.isEmpty(firstName.getText()))
            displayError(firstName,"This is a mandatory field!");

        if (TextUtils.isEmpty(lastName.getText()))
            displayError(lastName,"This is a mandatory field!");

        if (TextUtils.isEmpty(email.getText()))
            displayError(email,"This is a mandatory field!");
        else if (!isValidEmail(email.getText()))
            displayError(email,"Invalid e-mail!");

        if (TextUtils.isEmpty(password.getText()))
            displayError(password,"This is a mandatory field!");

        if (TextUtils.isEmpty(passwordConf.getText()))
            displayError(passwordConf,"This is a mandatory field!");

        if (TextUtils.isEmpty(location.getText()))
            displayError(location,"This is a mandatory field!");

        if ((!(TextUtils.isEmpty(password.getText())) && !(TextUtils.isEmpty(passwordConf.getText())))
             && (!(password.getText().toString().equals(passwordConf.getText().toString()))))
           displayError(passwordConf,"Password confirmation mismatch!");

        if (password.getText().toString().length() < 4)
            displayError(password, "Password must have at least 4 characters");


        if (!hasErrors) {

            String first_name, last_name, e_mail, password_1, password_2, user_location;

            first_name      = SXP_Utils.prepareParameter(firstName.getText().toString());
            last_name       = SXP_Utils.prepareParameter(lastName.getText().toString());
            e_mail          = SXP_Utils.prepareParameter(email.getText().toString());
            password_1      = SXP_Utils.prepareParameter(password.getText().toString());
            password_2      = SXP_Utils.prepareParameter(passwordConf.getText().toString());
            user_location   = SXP_Utils.prepareParameter(location.getText().toString());


            String URL_final = URL+
                    "first_name="+first_name+"&"+
                    "last_name="+last_name+"&"+
                    "email="+e_mail+"&"+
                    "password="+password_1+"&"+
                    "password_conf="+password_2+"&"+
                    "location="+user_location;

            Log.d("SXP_URL", URL_final);


            setProgressBarIndeterminateVisibility(true);

            sxp_request = new SXP_Request("SXP/Register", this);
            sxp_request.execute(URL_final);

        }

        Log.d("Sxp/register", "Inputs: " + firstName.getText() + ", " + lastName.getText() + ", " + email.getText() + ", " + password.getText() + ", " + passwordConf.getText() + ", " + location.getText());
        Log.d("Sxp/register", "Errors:" + hasErrors);



    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
