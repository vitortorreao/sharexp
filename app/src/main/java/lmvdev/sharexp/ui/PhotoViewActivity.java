package lmvdev.sharexp.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import lmvdev.sharexp.R;
import lmvdev.sharexp.model.Product;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class PhotoViewActivity extends Activity {

    Product product;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);

       getActionBar().setDisplayHomeAsUpEnabled(true);
       getActionBar().setDisplayShowHomeEnabled(true);


       product = (Product) getIntent().getExtras().getSerializable("product");

        ImageView photoView = (PhotoView) findViewById(R.id.photoView_photo);
        PhotoViewAttacher attacher;

        if (product != null) {
            Picasso.with(this)
                    .load(product.getPictureUrl())
                    .into(photoView);

            attacher = new PhotoViewAttacher(photoView);

            this.setTitle(product.getName());

        } else {
            Toast.makeText(this,"There was an error with the photo view, please try again!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onMenuItemSelected(featureId, item);
    }

}
