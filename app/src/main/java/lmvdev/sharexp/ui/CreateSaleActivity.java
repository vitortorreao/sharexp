package lmvdev.sharexp.ui;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lmvdev.sharexp.R;
import lmvdev.sharexp.utils.SXP_ToastDisplayer;

public class CreateSaleActivity extends ActionBarActivity implements ProgressBarActivity, DatePickerDialog.OnDateSetListener {

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

    private static final String formUrl = "http://share-xp.appspot.com/new_sale";
    public static final String IMAGE_URL = "url";

    private static final int imageMaxHeight = 2000;
    private static final int imageMaxWidth = 2000;


    SharedPreferences mSharedPreferences;

    EditText endDate;

    boolean hasErrors;
    private Uri fileUri;

    private String[] merchantNames;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setProgressBarIndeterminateVisibility(false);

        setContentView(R.layout.activity_create_sale);

        ImageView productPicture = (ImageView) findViewById(R.id.createSale_productPicture);

        if ((savedInstanceState != null) && (savedInstanceState.containsKey(IMAGE_URL))) {
            fileUri = Uri.fromFile(new File(savedInstanceState.getString(IMAGE_URL)));
            productPicture.setImageURI(fileUri);
        } else {
            productPicture.setImageResource(R.drawable.ic_add_picture);
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        endDate = (EditText) findViewById(R.id.createSale_endDate);

        endDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datePickerDialog.show(getSupportFragmentManager(), "datepicker");
                }
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                datePickerDialog.show(getSupportFragmentManager(), "datepicker");

            }
        });


        hasErrors = false;

        mSharedPreferences = getSharedPreferences(MainActivity.PREFS, MODE_PRIVATE);

        Ion.with(this)
                .load("http://share-xp.appspot.com/merchants")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        if (e == null) {

                            Log.d("Sxp/Autocomplete", "Result: " + result.toString());

                            if (result.get("status").getAsBoolean()) {

                                Log.d("Sxp/Autocomplete","1");

                                final JsonArray merchantInfo = result.get("merchants").getAsJsonArray();
                                final int numberOfMerchants = merchantInfo.size();

                                merchantNames = new String[numberOfMerchants];

                                Log.d("Sxp/Autocomplete","Merchant Info: " + merchantInfo.toString());


                                for (int i = 0; i < numberOfMerchants; i++) {

                                    JsonObject merchant = merchantInfo.get(i)
                                            .getAsJsonObject();

                                    String name = merchant.get("name").getAsString();

                                    String location = merchant.get("location")
                                            .getAsString();

                                    merchantNames[i] = name + ", " + location;
                                    Log.d("Sxp/Autocomplete", merchantNames[i].toString());

                                }

                                setAutocomplete();

                                Log.d("Sxp/Autocomplete", "Merchants: " + merchantNames.toString());

                            }


                        } else {

                            Log.d("Sxp/Autocomplete", "Error e: " + e.getMessage());

                        }

                    }
                });


    }

    public void setAutocomplete() {

        ArrayAdapter<String> autoCompMerchantAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, merchantNames);
        final AutoCompleteTextView merchants = (AutoCompleteTextView)
                findViewById(R.id.createSale_MerchantName);
        merchants.setAdapter(autoCompMerchantAdapter);
        merchants.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(CreateSaleActivity.this, (String) parent.getItemAtPosition(position),Toast.LENGTH_SHORT).show();

                String[] infoMerchant = ((String) parent.getItemAtPosition(position)).split(", ");


                String name = infoMerchant[0];
                String address = infoMerchant[1];
                String city = infoMerchant[infoMerchant.length - 2] + ", "
                        + infoMerchant[infoMerchant.length - 1];

                merchants.setText(name);

                TextView addressTextView = (TextView) findViewById(R.id.createSale_MerchantAddress);
                TextView cityTextView = (TextView) findViewById(R.id.createSale_MerchantCity);

                addressTextView.setText(address);
                cityTextView.setText(city);
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if(fileUri != null) {
            outState.putString(IMAGE_URL, fileUri.getPath());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }


    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("Sxp/Camera", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void shrinkImage(String filename, int width, int height) {

        Log.i("Sxp/ShrinkImage", "Begin Image Shrink Process...");

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(filename, bmpFactoryOptions);

        Log.d("Sxp/ShrinkImage", "Out Height: "+bmpFactoryOptions.outHeight);
        Log.d("Sxp/ShrinkImage", "Out width: "+bmpFactoryOptions.outWidth);

        int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight/(float) height);
        int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth/(float) width);

        Log.d("Sxp/ShrinkImage", "Height Ratio: "+heightRatio);
        Log.d("Sxp/ShrinkImage", "Width Ratio: "+widthRatio);

        if (heightRatio > 1 || widthRatio > 1) {
            if(heightRatio > widthRatio) {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;

        bitmap = BitmapFactory.decodeFile(filename, bmpFactoryOptions);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        try {

            FileOutputStream fo = new FileOutputStream(filename);
            fo.write(bytes.toByteArray());
            fo.close();

            Log.i("Sxp/ShrinkImage", "Image Shrinked!");

        } catch (FileNotFoundException e) {
            Log.e("Sxp/ShrinkImage", e.getMessage());
        } catch (IOException e) {
            Log.e("Sxp/ShrinkImage", e.getMessage());
        }

    }

    public void addPicture(View view) {

        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

        // start the image capture Intent
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent
                Log.d("Sxp/Camera","Image saved to:\n" + fileUri.toString());

                //Shrink Image
                shrinkImage(fileUri.getPath(), imageMaxWidth, imageMaxHeight);

                ImageView productPicture = (ImageView) findViewById(R.id.createSale_productPicture);
                productPicture.setImageURI(fileUri);
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
                Log.d("Sxp/Camera", "User cancelled camera");
            } else {
                // Image capture failed, advise user
                Log.d("Sxp/Camera", "Image Capture failed");
            }
        }
    }

    public void createSale(View view) {

        hasErrors = false;

        TextView productName = (TextView) findViewById(R.id.createSale_productName);
        TextView originalPrice = (TextView) findViewById(R.id.createSale_originalPrice);
        TextView discountDescription = (TextView) findViewById(R.id.createSale_discountDescription);
        TextView merchantName = (TextView) findViewById(R.id.createSale_MerchantName);
        TextView merchantAddress = (TextView) findViewById(R.id.createSale_MerchantAddress);
        TextView merchantCity = (TextView) findViewById(R.id.createSale_MerchantCity);
        TextView endDate = (TextView) findViewById(R.id.createSale_endDate);

        if (TextUtils.isEmpty(productName.getText()))
            displayError(productName,"This is a mandatory field!");

        if (TextUtils.isEmpty(originalPrice.getText()))
            displayError(originalPrice,"This is a mandatory field!");
        else {
            try {
                String price = originalPrice.getText().toString().replace(",",".");
                Double.parseDouble(price);
                originalPrice.setText(price);
            } catch(Exception e) {
                displayError(originalPrice,"Please enter a valid value, e.g 3.50");
            }
        }

        if (TextUtils.isEmpty(discountDescription.getText()))
            displayError(discountDescription,"This is a mandatory field!");

        if (TextUtils.isEmpty(merchantName.getText()))
            displayError(merchantName,"This is a mandatory field!");

        /*
        if (TextUtils.isEmpty(merchantAddress.getText()))
            displayError(merchantAddress,"This is a mandatory field!");
        */

        if (TextUtils.isEmpty(merchantCity.getText()))
            displayError(merchantCity,"This is a mandatory field!");

        if (TextUtils.isEmpty(endDate.getText()))
            displayError(endDate,"This is a mandatory field!");
        else if (!endDate.getText().toString().matches("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))" +
                "\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})" +
                "$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579]" +
                "[26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)" +
                "(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$")) {
            displayError(endDate, "Please enter a valid date!");
        }

        if (!hasErrors) {

            ((Button) findViewById(R.id.createSale_createButton)).setEnabled(false);

            setProgressBarIndeterminateVisibility(true);

            Ion.with(CreateSaleActivity.this)
                    .load(formUrl)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String s) {

                            Pattern pattern = Pattern.compile("action=\"(.*)\"");
                            Matcher matcher = pattern.matcher(s);
                            String uploadUrl = "";
                            if (matcher.find()) {
                                uploadUrl = matcher.group(1);
                                uploadUrl = uploadUrl.split("\" ")[0];
                            }

                            Log.d("Sxp/CreateSaleGet", uploadUrl);

                            Log.d("Sxp/CreateSale", "Beginning POST request ");

                            TextView productName = (TextView) findViewById(R.id.createSale_productName);
                            TextView originalPrice = (TextView) findViewById(R.id.createSale_originalPrice);
                            TextView discountDescription = (TextView) findViewById(R.id.createSale_discountDescription);
                            TextView merchantName = (TextView) findViewById(R.id.createSale_MerchantName);
                            TextView merchantAddress = (TextView) findViewById(R.id.createSale_MerchantAddress);
                            TextView merchantCity = (TextView) findViewById(R.id.createSale_MerchantCity);
                            TextView endDate = (TextView) findViewById(R.id.createSale_endDate);
                            TextView hashTags = (TextView) findViewById(R.id.createSale_hashTag);

                            String product_name, original_price, discount_description, merchant_name,
                                    merchant_address, end_date, hash_tags;

                            product_name = productName.getText().toString();
                            original_price = originalPrice.getText().toString();
                            discount_description = discountDescription.getText().toString();
                            merchant_name = merchantName.getText().toString();
                            merchant_address = merchantAddress.getText().toString() + ", "+
                                    merchantCity.getText().toString();
                            end_date = endDate.getText().toString();
                            hash_tags = hashTags.getText().toString();

                            String fileName = "";
                            File sourceFile = null;
                            if(fileUri != null) {
                                fileName = fileUri.getPath();

                                sourceFile = new File(fileName);

                                if(!sourceFile.isFile()) {
                                    Log.e("Sxp/CreateSale", "File does not exist");
                                    setProgressBarIndeterminateVisibility(false);
                                    ((Button) findViewById(R.id.createSale_createButton)).setEnabled(true);
                                    return;
                                }
                            }

                            try {

                                String cookie = mSharedPreferences
                                        .getString(MainActivity.PREF_COOKIE, null);

                                if(cookie == null) {
                                    runOnUiThread(
                                            new SXP_ToastDisplayer(CreateSaleActivity.this,
                                                    "You need to be logged in!"));

                                    setProgressBarIndeterminateVisibility(false);
                                    ((Button) findViewById(R.id.createSale_createButton)).setEnabled(true);
                                    return;
                                }

                                if(fileUri == null) {
                                    Ion.with(CreateSaleActivity.this)
                                            .load("POST", uploadUrl)
                                            .setHeader("Cookie", "id="+cookie)
                                            .setMultipartParameter("product_name", product_name)
                                            .setMultipartParameter("product_price", original_price)
                                            .setMultipartParameter("discount", discount_description)
                                            .setMultipartParameter("merchant_name", merchant_name)
                                            .setMultipartParameter("merchant_location", merchant_address)
                                            .setMultipartParameter("end_date", end_date)
                                            .setMultipartParameter("hashtags", hash_tags)
                                            .asString()
                                            .setCallback(new FutureCallback<String>() {
                                                @Override
                                                public void onCompleted(Exception e, String s) {
                                                    Log.d("Sxp/CreateSale", s);
                                                    onTaskFinish(s);
                                                }
                                    });
                                } else {
                                    Ion.with(CreateSaleActivity.this)
                                            .load("POST", uploadUrl)
                                            .setHeader("Cookie", "id="+cookie)
                                            .setMultipartParameter("product_name", product_name)
                                            .setMultipartParameter("product_price", original_price)
                                            .setMultipartParameter("discount", discount_description)
                                            .setMultipartParameter("merchant_name", merchant_name)
                                            .setMultipartParameter("merchant_location", merchant_address)
                                            .setMultipartParameter("end_date", end_date)
                                            .setMultipartParameter("hashtags", hash_tags)
                                            .setMultipartFile("file", sourceFile)
                                            .asString()
                                            .setCallback(new FutureCallback<String>() {
                                                @Override
                                                public void onCompleted(Exception e, String s) {
                                                    Log.d("Sxp/CreateSale", s);
                                                    onTaskFinish(s);
                                                }
                                    });
                                }

                            } catch (Exception exc) {
                                Log.e("Sxp/CreateSale", exc.getMessage());
                                setProgressBarIndeterminateVisibility(false);
                                ((Button) findViewById(R.id.createSale_createButton)).setEnabled(true);
                            }
                        }

                    });


        }


    }

    public void displayError(TextView textView, String message) {
        textView.setError(message);
        hasErrors = true;
    }

    @Override
    public void setProgressBarFalse() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    @Override
    public void setProgressBarTrue() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(true);
            }
        });
    }

    @Override
    public void onTaskFinish(String result) {

        setProgressBarIndeterminateVisibility(false);
        ((Button) findViewById(R.id.createSale_createButton)).setEnabled(true);

        try {
            JSONObject json = new JSONObject(result);
            if(json.getBoolean("status")) {

                runOnUiThread(new SXP_ToastDisplayer(this, "Sale was successfully created!"));

                //Call Home Activity
                //Intent homeIntent = new Intent(this, HomeActivity.class);
                //homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //startActivity(homeIntent);
                finish();

            } else {
                runOnUiThread(new SXP_ToastDisplayer(this, json.getString("reason")));
            }
        } catch (JSONException e) {
            runOnUiThread(new SXP_ToastDisplayer(this, "There was a problem on the server. Try again later"));
        }

        Log.d("Sxp/CreateSale","Result: "+ result.toString());

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {

        Calendar cal = Calendar.getInstance();
        cal.set(year,month,day);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        endDate.setText(dateFormat.format(cal.getTime()));
    }
}
