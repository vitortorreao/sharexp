package lmvdev.sharexp.ui.adapter;

import lmvdev.sharexp.model.Notification;
import lmvdev.sharexp.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Vector;

public class NotificationsListViewAdapter extends ArrayAdapter<Notification> {

    private final Context context;
    private final Vector<Notification> values;

    public NotificationsListViewAdapter(Context context, Vector<Notification> values) {
        super(context, R.layout.adapter_notifications , values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_notifications, parent, false);
        TextView messageTextView = (TextView) rowView.findViewById(R.id.notification_label);
        TextView dateTextView = (TextView) rowView.findViewById(R.id.notification_secondLine);
        ImageView icon = (ImageView) rowView.findViewById(R.id.notification_icon);
        messageTextView.setText(values.get(position).getMessage());
        dateTextView.setText(values.get(position).getDate().toString());

        switch (values.get(position).getType()) {
            case Notification.NOTIFICATION_SALES:
                icon.setImageResource(R.drawable.ic_sales_blue);
                break;
            case Notification.NOTIFICATION_MEALS:
                icon.setImageResource(R.drawable.ic_sales_blue);
                break;
            case Notification.NOTIFICATION_TRAVELS:
                icon.setImageResource(R.drawable.ic_sales_blue);
                break;
            default:
                icon.setImageResource(R.drawable.ic_sales_blue);
                break;
        }

        return rowView;
    }

}
