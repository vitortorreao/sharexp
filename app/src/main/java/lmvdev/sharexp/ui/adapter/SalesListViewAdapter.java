package lmvdev.sharexp.ui.adapter;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Vector;

import lmvdev.sharexp.R;
import lmvdev.sharexp.model.Sale;
import lmvdev.sharexp.model.Utils;

public class SalesListViewAdapter extends ArrayAdapter<Sale> {

    private final Context context;
    private final Vector<Sale> values;
    private final Location currentLocation;

    public SalesListViewAdapter(Context context, Vector<Sale> values, Location currentLocation) {
        super(context, R.layout.adapter_sales , values);
        this.context = context;
        this.values = values;
        this.currentLocation = currentLocation;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_sales, parent, false);
        TextView productDescription = (TextView) rowView.findViewById(R.id.sale_label);
        TextView saleDescription = (TextView) rowView.findViewById(R.id.sale_secondLine);
        TextView distance = (TextView) rowView.findViewById(R.id.sale_distance);
        ImageView icon = (ImageView) rowView.findViewById(R.id.sale_icon);
        productDescription.setText(values.get(position).getProduct().getName());
        if (values.get(position).hasOriginalPrice())
            saleDescription.setText("Before:£" + String.format("%.2f",
                    values.get(position).getOriginalPrice()) + " | Now: " +
                    values.get(position).getDiscountDescription());
        else
            saleDescription.setText(values.get(position).getDiscountDescription());
        Double dist = Utils.distance(currentLocation.getLatitude(), currentLocation.getLongitude(),
                values.get(position).getMerchant().getLatitude(),
                values.get(position).getMerchant().getLongitude(), 'K');
        if (dist < 1.0) {
            dist *= 1000;
            distance.setText(String.format("%.0f",dist) + "m");
        } else {
            distance.setText(String.format("%.2f",dist) + "km");
        }

        Log.d("Sxp/SalesAdapter","Picture Url: " + values.get(position).getProduct().getPictureUrl());

        if (values.get(position).getProduct().getPictureUrl() != null) {
            Picasso.with(getContext())
                    .load(values.get(position).getProduct().getPictureUrl())
                    .resize(50, 50)
                    .centerCrop()
                    .into(icon);
        } else {
            icon.setImageResource(R.drawable.ic_sales_blue);
        }



        return rowView;
    }

}
