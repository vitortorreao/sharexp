package lmvdev.sharexp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Date;
import java.util.Vector;

import lmvdev.sharexp.R;
import lmvdev.sharexp.ui.adapter.NotificationsListViewAdapter;
import lmvdev.sharexp.model.Notification;

public class NotificationsFragment extends Fragment {

    ListView notificationsList;
    NotificationsListViewAdapter adapter;
    Vector<Notification> notifications;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_notifications, container, false);

        notificationsList = (ListView) rootView.findViewById(R.id.notification_listView);

        getUserNotifications();

        adapter = new NotificationsListViewAdapter(getActivity(), notifications);
        notificationsList.setAdapter(adapter);

        notificationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                Toast.makeText(getActivity(), "You just clicked on " + notifications.get(position).getMessage() + " notification.", Toast.LENGTH_SHORT).show();
            }

        });

        return rootView;
    }

    public void getUserNotifications() {

        notifications = new Vector<Notification>();
        notifications.add(new Notification(Notification.NOTIFICATION_SALES, "Message 1", new Date()));
        notifications.add(new Notification(Notification.NOTIFICATION_SALES, "Message 2", new Date()));
        notifications.add(new Notification(Notification.NOTIFICATION_MEALS, "Message 3", new Date()));
        notifications.add(new Notification(Notification.NOTIFICATION_MEALS, "Message 4", new Date()));
        notifications.add(new Notification(Notification.NOTIFICATION_TRAVELS, "Message 5", new Date()));
        notifications.add(new Notification(Notification.NOTIFICATION_TRAVELS, "Message 6", new Date()));

    }

}