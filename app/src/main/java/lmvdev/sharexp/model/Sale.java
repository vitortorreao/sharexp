package lmvdev.sharexp.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Sale implements Serializable {

    private boolean valid;
    private Double originalPrice;
    private String discountDescription;
    private Date endDate;
    private Product product;
    private Merchant merchant;
    private String[] hashtags;
    private long id;
    private Person owner;

    public Sale(Double originalPrice, String discountDescription, Date endDate, String productName,
                String productPicture, String merchantName, String merchantAddress,
                String merchantCity, long id, String ownerName, String ownerEmail, String owner,
                String ownerLocation, long ownerId ) {
        this.originalPrice = originalPrice;
        this.product = new Product(productName, productPicture);
        this.discountDescription = discountDescription;
        this.merchant = new Merchant(merchantName, merchantAddress + ", " + merchantCity);
        this.endDate = endDate;
        valid = true;
        this.id = id;
        this.owner = new Person(ownerId, ownerEmail, ownerName, ownerLocation);
    }

    public Sale(Double originalPrice, String discount, Date endDate, Product product,
                Merchant merchant, long id, Person owner) {
        this.originalPrice = originalPrice;
        this.discountDescription = discount;
        this.endDate = endDate;
        this.product = product;
        this.merchant = merchant;
        valid = true;
        this.id = id;
        this.owner = owner;
    }

    public Sale(JSONObject json) {
        try {

            this.merchant = new Merchant(json.getJSONObject("merchant"));
            this.product = new Product(json.getJSONObject("product"));

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            this.endDate = formatter.parse(json.getString("end_date"));

            this.originalPrice = json.getDouble("price");
            this.discountDescription = json.getString("discount");

            this.valid = json.getBoolean("valid");

            this.id = Long.parseLong(json.getString("id"));

            this.owner = new Person(json.getJSONObject("owner"));

            JSONArray jHashtags = json.getJSONArray("hashtags");
            this.hashtags = new String[jHashtags.length()];
            for(int i = 0; i < jHashtags.length(); i++) {
                this.hashtags[i] = jHashtags.getString(i);
            }

        } catch(Exception e) {
            return;
        }
    }

    public boolean hasOriginalPrice() {
        if (originalPrice > 0)
            return true;
        return false;

    }

    public boolean isValid() {
        return valid;
    }

    public Double getOriginalPrice() {
        return originalPrice;
    }

    public String getDiscountDescription() {
        return discountDescription;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Product getProduct() {
        return product;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public long getId() {
        return id;
    }

    public Person getOwner() {
        return owner;
    }

    public String[] getHashtags() {
        return hashtags;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setOriginalPrice(Double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setDiscountDescription(String discountDescription) {
        this.discountDescription = discountDescription;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public void setHashtags(String[] hashtags) {
        this.hashtags = hashtags;
    }
}
