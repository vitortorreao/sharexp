package lmvdev.sharexp.model;

import android.graphics.Picture;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Vector;

public class Merchant implements Serializable {

    private String name;
    private String address;
    private double latitude;
    private double longitude;

    private Vector<Product> listOfProducts;

    public Merchant(String name, String address) {
        this.name = name;
        this.address = address;
        listOfProducts = null;
    }

    public Merchant(JSONObject json) {
        try {
            this.name = json.getString("name");
            this.address = json.getString("location");
            this.latitude = json.getJSONObject("geo_location").getDouble("lat");
            this.longitude = json.getJSONObject("geo_location").getDouble("lon");
        }catch(Exception e) {
            return;
        }
    }

    public void addProduct(Product product) {
        if (listOfProducts == null)
            listOfProducts = new Vector<Product>();
        listOfProducts.add(product);
    }

    public void addProduct(String productName, String picture) {
        if (listOfProducts == null)
            listOfProducts = new Vector<Product>();
        listOfProducts.add(new Product(productName, picture));
    }

    public void addProduct(String productName) {
        if (listOfProducts == null)
            listOfProducts = new Vector<Product>();
        listOfProducts.add(new Product(productName));
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getLocationWithoutSpaces () {
        String newLocation;
        newLocation = address.replace(" ","+");
        newLocation = newLocation.replace(",","+");
        newLocation = newLocation.replace("-","+");
        newLocation = newLocation.replace(".","+");

        return newLocation;
    }

    public Vector<Product> getListOfProducts() {
        return listOfProducts;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setListOfProducts(Vector<Product> listOfProducts) {
        this.listOfProducts = listOfProducts;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}