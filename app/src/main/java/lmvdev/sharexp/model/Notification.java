package lmvdev.sharexp.model;


import java.util.Date;

public class Notification {

    public static final int NOTIFICATION_SALES = 1;
    public static final int NOTIFICATION_MEALS = 2;
    public static final int NOTIFICATION_TRAVELS = 3;


    public Notification(int type, String message, Date date) {
        this.type = type;
        this.message = message;
        this.date = date;
    }

    public Notification(int type, String message, Date date, Person destination, Person origin) {
        this.type = type;
        this.message = message;
        this.date = date;
        this.destination = destination;
        this.origin = origin;
    }

    private int type;
    private String message;
    private Date date;
    private Person destination;
    private Person origin;

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public Person getDestination() {
        return destination;
    }

    public Person getOrigin() {
        return origin;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDestination(Person destination) {
        this.destination = destination;
    }

    public void setOrigin(Person origin) {
        this.origin = origin;
    }
}
