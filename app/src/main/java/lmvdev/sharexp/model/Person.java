package lmvdev.sharexp.model;

import org.json.JSONObject;

import java.io.Serializable;

public class Person implements Serializable{

    private long id;
    private String email;
    private String name;
    private String location;

    public Person(long id, String email, String name, String location) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.location = location;
    }

    public Person(long id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public Person(JSONObject json) {
        try {
            this.id = Long.parseLong(json.getString("id"));
            this.email = json.getString("email");
            this.name = json.getString("name");
            this.location = json.getString("location");
        } catch (Exception e) {

        }
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
