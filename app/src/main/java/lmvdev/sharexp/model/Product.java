package lmvdev.sharexp.model;

import org.json.JSONObject;

import java.io.Serializable;

public class Product implements Serializable {

    private String name;
    String pictureUrl;

    public Product(String name) {
        this.name = name;
        pictureUrl = null;
    }

    public Product(String name, String pictureUrl) {
        this.name = name;
        this.pictureUrl = pictureUrl;
    }

    public Product(JSONObject json) {
        try {
            this.name = json.getString("name");
            this.pictureUrl = json.getString("image_url");
            if ((this.pictureUrl == null) || (this.pictureUrl.equals("null"))) {
                this.pictureUrl = null;
            }
        } catch(Exception e) {
            return;
        }

    }

    public boolean hasPictureUrl() {
        if (pictureUrl != null)
            return true;
        return false;
    }

    public String getName() {
        return name;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
