package lmvdev.sharexp.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SXP_Utils {

    public static String prepareParameter(String parameter) {
        String result = "";
        try {
            result = URLEncoder.encode(parameter ,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

}
