package lmvdev.sharexp.utils;

import android.content.Context;
import android.widget.Toast;

public class SXP_ToastDisplayer implements Runnable {

    private Context context;
    private String message;

    public SXP_ToastDisplayer(Context context, String message) {
        this.context = context;
        this.message = message;
    }

    @Override
    public void run() {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
