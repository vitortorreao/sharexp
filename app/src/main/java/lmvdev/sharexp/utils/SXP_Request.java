package lmvdev.sharexp.utils;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import lmvdev.sharexp.ui.ProgressBarActivity;

public class SXP_Request extends AsyncTask<String, String, String> {

    private String cookie;
    private String log_tag;
    private ProgressBarActivity activity;

    public SXP_Request(String log_tag, ProgressBarActivity activity) {
        cookie = null;
        this.log_tag = log_tag;
        this.activity = activity;
    }

    public SXP_Request(String log_tag, ProgressBarActivity activity, String cookie) {
        this.cookie = cookie;
        this.log_tag = log_tag;
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... uri) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            HttpGet request = new HttpGet(uri[0]);
            if (cookie != null)
                request.addHeader("Cookie", "id="+cookie);
            response = httpclient.execute(request);
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){


               if(response.getFirstHeader("Set-Cookie") != null) {

                   //Saves the cookie in the attribute
                   cookie = response.getFirstHeader("Set-Cookie").getValue()
                           .split(";")[0]
                           .split("=")[1];

               }



                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                responseString = out.toString();
                Log.d(this.log_tag, responseString);
                this.activity.setProgressBarFalse();
                this.activity.onTaskFinish(responseString);
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
            e.printStackTrace();
        } catch (IOException e) {
            //TODO Handle problems..
            e.printStackTrace();
        }
        return responseString;
    }

    public String getCookie() {
        return cookie;
    }
}
